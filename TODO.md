# :boom: :boom: :boom: TO DO LIST :boom: :boom: :boom:

## TEAMS

**CSS et organisation** :cow2:
- Théo
- MP
- Amandine
- Hervé

**PHP/Technique** :goat:
- Natalia
- Elodie

# :zap: :zap: :zap: :zap: :zap: :zap: :zap: :zap: :zap: 

## TO DO

**ORGANISATION/CSS**
- [ ] Création de dossiers pour chaque lien dans le Zotero (créations de sous-dossiers, quand c'est nécessaire?)
- [ ] Réorganisation des tags (sélection), création de tags (prénoms?)
- [ ] Pictos personne
- [ ] Pictos type de docs
- [ ] Déploiement des liens / éléments associés aux titres, éléments graphiques, notes, images...
- [ ] Maquette InDesign, présentation du site


**TECHNIQUE**
- [x] Faire apparaître les "attachments" dans les sous-dossiers -> voir avec Lionel si autre technique possible (vendredi 10 décembre)
- [x] Images à afficher (iframe ne fonctionne pas) Les images elles s'ouvrent si on les sauvegarde depuis internet avec le plugin Zotero. -> quand on les place dans les notes ça s'affiche, autre moyen? voir avec Lionel
- [ ] Ouvrir différents formats (e.g. .epub, pdf, md, docx, txt, ils ne s'ouvrent pas) -> tester dans les notes
- [ ] Outil de recherche par ? (auteur.rice, titre, mots-cléfs etc), à définir
- [ ] Règler le problème des doublons
