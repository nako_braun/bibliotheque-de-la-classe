<?php
include('inc/header.php');
include('inc/nav.php');

// on recupère l'API de notre Zotero
$url = file_get_contents('https://api.zotero.org/groups/4412339/items?limit=1000');
  // file_get_contents() permet de retourner le fichier dans une chaîne de caractères

// on crée un tableau associatif de nos données
$array = json_decode($url, TRUE);
  // JSON est un format de fichier standard ouvert, c'est un format d'échange de données qui utilise du texte lisible par l'homme
    // TRUE = les objets JSON seront renvoyés sous forme de tableaux associatifs
    // FALSE = les objets JSON seront renvoyés sous forme d'objets.
    // NULL = les objets JSON sont renvoyés sous forme de tableaux associatifs ou d'objets

$items = array_merge($array);

// var_dump($items);

  foreach ($items as $item) {

  $data = $item['data'];
  $meta = $item['meta'];
  
    $title = $data['title'];
    $link = $data['url'];
    $date = substr($data['dateAdded'], 0, 10);
    $abstractNote = $data['abstractNote'];
    $language = $data['language'];
    $format = $data['itemType'];
    $tags = $data['tags'];
    $note = $data['note'];
    $attachement = $data['attachement'];
    $attachementUrl = $data['attachement']['url'];

    $user = $meta['createdByUser']['username'];
    $shortTitle = $data['shortTitle'];


    echo '<div class="item">';
    echo '<section class="info">';
    ?>

    <span class="date">
      <svg height="22" width="140">
        <path height="auto" fill="none" id="curve" d="m 3.632326,21.554668 c 13.850974,-2.713501 19.167278,-4.663459 28.9579,-5.047154 13.275673,-0.520276 26.395491,3.869911 39.680816,3.749799 7.778558,-0.07033 15.411644,-2.497371 23.189638,-2.61522 11.23048,-0.170167 22.35094,2.875937 33.58212,2.759917 11.9099,-0.123021 23.63603,-3.932832 35.54349,-3.661738 11.35769,0.258579 14.30407,0.847124 33.70838,5.031969"/>
        <text>
          <textPath height="auto" xlink:href="#curve" style="fill: #8f6c8dff;">
            <?php echo $date?>
          </textPath>
        </text>
      </svg>
    </span>

    <?php

    echo '<a class="link" href="' . $link . '"target="_blank">' . ($shortTitle ? $shortTitle : $title . '') . '</a>';
    
    echo '<span class="user">' . " · " . $user . '</span>';
    // echo '<span class="language">' . " ". $language . '</span>';
    echo '<br>';

    foreach($tags as $tag){
      echo '<span class="tags">'. '#'. $tag['tag']. ' ' . '</span>';;
    }
    echo '<br>';

    echo '<span class="abstractNote">' . $abstractNote . '</span>';
    echo '<span class="note">' . $note . '</span>';
    echo '<span class="attachement">' . $attachement . '</span>';
  

    echo '</section>';
  echo '</div>';
  }

   include('inc/footer.php');
?>
